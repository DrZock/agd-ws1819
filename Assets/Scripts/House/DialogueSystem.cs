﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DialogueSystem : MonoBehaviour
{

    public Text nameText;
    public Text dialogueText;

    public GameObject dialogueGUI;
    public Transform dialogueBoxGUI;
    public Text questGUI;

    public ChoiceManager choiceManager;
    public NPC npc;

    public CapsuleCollider collider;
    

    public float letterDelay = 0.01f;
    public float letterMultiplier = 0.5f;

    public KeyCode DialogueInput = KeyCode.F;

    public string Names;

    public string[] dialogueLines;

    public bool letterIsMultiplied = false;
    public bool dialogueActive = false;
    public bool dialogueEnded = false;
    public bool outOfRange = true;

    public AudioClip audioClip;
    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        dialogueText.text = "";
    }

    void Update()
    {

    }

    public void EnterRangeOfNPC()
    {
        outOfRange = false;
        dialogueGUI.SetActive(true);
        if (dialogueActive == true)
        {
            dialogueGUI.SetActive(false);
        }
    }

    public void NPCName()
    {
        outOfRange = false;
        dialogueBoxGUI.gameObject.SetActive(true);
        nameText.text = Names;
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (!dialogueActive)
            {
                dialogueActive = true;
                StartCoroutine(StartDialogue());
            }
        }
        StartDialogue();
    }

    private IEnumerator StartDialogue()
    {
        if (outOfRange == false)
        {
            int dialogueLength = dialogueLines.Length;
            int currentDialogueIndex = 0;

            while (currentDialogueIndex < dialogueLength || !letterIsMultiplied)
            {
                if (!letterIsMultiplied)
                {
                    letterIsMultiplied = true;
                    StartCoroutine(DisplayString(dialogueLines[currentDialogueIndex++]));

                    if (currentDialogueIndex >= dialogueLength)
                    {
                        dialogueEnded = true;
                    }
                }
                yield return 0;
            }

            while (true)
            {
                if (Input.GetKeyDown(DialogueInput) && dialogueEnded == false)
                {
                    break;
                }
                yield return 0;
            }
            dialogueEnded = false;
            dialogueActive = false;
            DropDialogue();
        }
    }

    private IEnumerator DisplayString(string stringToDisplay)
    {
        if (outOfRange == false)
        {
            int stringLength = stringToDisplay.Length;
            int currentCharacterIndex = 0;
            bool choice = false;
            bool next = true;
            int progress = 0;

            if (stringToDisplay.Contains("[Auswahl")){          //Choice generic 
                choice = true;           
            }

            if (stringToDisplay.Contains("Prost mein Freund.")){                  //Charlie 1
                progress = 1;
            }           
            if (stringToDisplay.Contains("Haha, nun bist du ja da."))                   //Amy 1
            {
                progress = 2;
            }
            if (stringToDisplay.Contains("und jetzt mach schon."))                   //Charlie 2
            {
                progress = 3;
            }
            if (stringToDisplay.Contains("Ehrlisch"))                   //Amy 2a /Remove Torben
            {
                progress = 4;
            }
            if (stringToDisplay.Contains("so schlecht ist."))               //Amy 2b
            {
                progress = 5;
            }
            if (stringToDisplay.Contains("DFSDFSDGSDGSG"))               //Amy 3 / DriveStart
            {
                progress = 6;
            }

            dialogueText.text = "";

            while (currentCharacterIndex < stringLength)
            {
                dialogueText.text += stringToDisplay[currentCharacterIndex];
                currentCharacterIndex++;
                //if (dialogueText.text.Contains("[Auswahl")) choice = true;
                if (currentCharacterIndex < stringLength)
                {
                    if (Input.GetKey(DialogueInput))
                    {
                        yield return new WaitForSeconds(letterDelay * letterMultiplier);

                        if (audioClip) audioSource.PlayOneShot(audioClip, 0.5F);
                    }
                    else
                    {
                        yield return new WaitForSeconds(letterDelay);

                        if (audioClip) audioSource.PlayOneShot(audioClip, 0.5F);
                    }
                }
                else
                {
                    dialogueEnded = false;
                    break;
                }

                
            }
            while (next)
            {

                if(choice){
                    if (Input.GetKeyDown(KeyCode.Alpha1)){                      //Option 1
                        int i = 1;                       
                        makeChoice(stringToDisplay, i, npc);
                        choice = false;
                        break;

                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha2))                  //Option 2
                    {                 
                        int i = 2;
                        makeChoice(stringToDisplay, i, npc);
                        choice = false;
                        break;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha3))                  //Option 3
                    {                 
                        int i = 3;
                        makeChoice(stringToDisplay, i, npc);
                        choice = false;
                        break;
                    }
                    //yield return 0;

                }
                else if (progress == 1){    //Charlie 1
                    if (Input.GetKeyDown(DialogueInput)){
                        npc.turnOff[0].SetActive(false);
                        npc.turnOn[0].SetActive(true);
                        questGUI.text = "-Ich sollte mit den Leuten auf der Party reden" + '\n' + "-Wenn ich bereit bin sollte ich mit Amy reden, sie soll im Wohnzimmer sein";
                        break;
                    }
                }
                else if (progress == 2){    //Amy 1
                    if (Input.GetKeyDown(DialogueInput)){
                        npc.turnOff[0].SetActive(false);
                        npc.turnOn[0].SetActive(true);

                        questGUI.text = "-Ich sollte mit den Leuten auf der Party reden" + '\n' + "-Ich sollte nochmal zu Charlie, jetzt hat er sicherlich mehr Zeit zum reden";
                        break;
                    }
                }
                else if (progress == 3){    //Charlie 2
                    if (Input.GetKeyDown(DialogueInput)){
                        npc.turnOff[0].SetActive(false);
                        npc.turnOn[0].SetActive(true);
                        npc.turnOn[1].SetActive(true);

                        questGUI.text = "-Ich sollte mit den Leuten auf der Party reden" + '\n' + "-Ich sollte zu Amy, sie wollte nochmal was mit mir trinken, wahrscheinlich find ich sie an der Bar in der Küche";
                        break;
                    }
                }
                else if (progress == 4){    //Amy 2a / Remove Torben
                    if (Input.GetKeyDown(DialogueInput)){
                        npc.turnOff[1].SetActive(false);
                        break;
                    }
                }
                else if (progress == 5){    //Amy 2b
                    if (Input.GetKeyDown(DialogueInput)) {
                        npc.turnOff[0].SetActive(false);
                        npc.turnOn[0].SetActive(true);
                        npc.turnOn[1].SetActive(true);
                        questGUI.text = "-Ich sollte mit den Leuten auf der Party reden" + '\n' + "-Ich sollte  Amy suchen, an der Bar wirkte sie schon ziemlich betrunken";
                        break;
                    }
                }
                else if (progress == 6) {    //Amy 3 / StartDrive
                    if (Input.GetKeyDown(DialogueInput)) {

                        break;
                    }
                }
                else if (Input.GetKeyDown(DialogueInput))
                {                            //Next Sentence                
                    break;
                }
                yield return 0;
            }
            dialogueEnded = false;
            letterIsMultiplied = false;
            dialogueText.text = "";
        }
    }



    /************************ CHOIC-MANAGER ANFANG *****************************/

    private void makeChoice(string text, int answer, NPC npc){

        if (text.Contains("Auswahl01")){            //Nick
            switch (answer) {
            
                case 1:
                    npc.sentences[2] = "Nick" + '\n' + '\n' + "Gute Wahl, bis gleich.";        //+1 Bier
                    break;

                case 2:
                    npc.sentences[2] = "Nick" + '\n' + '\n' + "Sehr gut, ich hoffe Vodka ist Ok für dich, bis gleich.";    //+1 Shot
                    break;
                
                case 3:
                    npc.sentences[2] = "Nick" + '\n' + '\n' + "Ok, kein Problem, viel Spass noch auf.";            //Nichts
                    break;
            
            
            }
        }
        else if (text.Contains("Auswahl02")){       //Tim Studium, kein Einfluss
            switch (answer)
            {

                case 1:
                            //Nichts
                    break;

                case 2:
                            //Nichts
                    break;

                case 3:
                            //Nichts
                    break;


            }
        }
        else if (text.Contains("Auswahl03")){       //Tim Bierpong
            switch (answer)
            {

                case 1:
                                //Nichts
                    break;

                case 2:
                    npc.sentences[6] = "Tim" + '\n' + '\n' + "Puhh.... Gut gespielt, aber mit doppelter Biermenge haut doch schon ganz schön rein.";    //+ 1 Bierpong  (+2 Bier)
                    break;

                case 3:         //Zählt in dem Fall als "2" (Bierpong spielen)
                    
                    break;


            }
        }
        else if (text.Contains("Auswahl04")) {      //Charlie2, Mutdrink
            switch (answer) {

                case 1:             // +1 Bier
                    npc.sentences[4] = "Charlie" + '\n' + '\n' + "Klar, ich bin gleich wieder da ..." + '\n' + '\n' + "Prost... und jetzt mach schon.";            
                    break;

                case 2:             // +1 Schnaps
                    npc.sentences[4] = "Charlie" + '\n' + '\n' + "Mein Vater müsste eigntlich irgendwo noch nen guten Scotch versteckt haben, ich werd mal kurz suchen..." + '\n' + '\n' + "Prost... und jetzt mach schon.";
                    break;

                case 3:             // Nichts
                    npc.sentences[4] = "Charlie" + '\n' + '\n' + "Alles klar, und jetzt mach schon...";
                    break;


            }
        }
        else if (text.Contains("Auswahl05")) {      //Amy2, Bar, kein Einfluss
            switch (answer) {

                case 1:             // +1 Schnaps
                    
                    break;

                case 2:             // +1 Schnaps
                    
                    break;

                case 3:             // +1 Schnaps
                    
                    break;


            }
        }
        else if (text.Contains("Auswahl06")) {      //Vodkagruppe
            switch (answer) {

                case 1:             // Nichts
                    npc.sentences[2] = "Partygast" + '\n' + '\n' + "Langweiler...";
                    break;

                case 2:             // +1 Schnaps
                    npc.sentences[2] = "Partygast" + '\n' + '\n' + "Ex auf 3, 1... 2... 3!!!";
                    break;

                case 3:             // +1 Schnaps (selbe wie 2)
                    npc.sentences[2] = "Partygast" + '\n' + '\n' + "Ex auf 3, 1... 2... 3!!!";
                    break;


            }
        }
        else if (text.Contains("Auswahl07")) {      //Fahrt
            switch (answer) {

                case 1:             // Fahrt
                    
                    break;

                case 2:             // Fahrt
                    
                    break;

                case 3:             // Fahrt
                    
                    break;


            }
        } 



    }



    /************************ CHOIC-MANAGER ENDE *****************************/



    public void DropDialogue()
    {
        dialogueGUI.SetActive(false);
        dialogueBoxGUI.gameObject.SetActive(false);
        collider.enabled = false;
    }

    public void OutOfRange()
    {
        outOfRange = true;
        if (outOfRange == true)
        {
            letterIsMultiplied = false;
            dialogueActive = false;
            StopAllCoroutines();
            dialogueGUI.SetActive(false);
            dialogueBoxGUI.gameObject.SetActive(false);
        }
    }
}