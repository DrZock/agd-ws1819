﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour {

    public float inputDelay = 0.1f;
    public float forwardVel = 12;
    public float rotateVel = 100;

    public GameObject dialogueBoxGUI;


    Quaternion targetRotation;
    Rigidbody rbody;
    float forwardInput, turnInput;

    public Quaternion TargetRotation{
        get { return targetRotation; }
    }

    void Start() {
        targetRotation = transform.rotation;
        if (GetComponent<Rigidbody>())
            rbody = GetComponent<Rigidbody>();
        else
            Debug.LogError("No Ridged Body...");

        forwardInput = 0;
        turnInput = 0;
    }

    void GetInput() {
        forwardInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");
    }

    void Update() {
        GetInput();
        Turn();

        /*if (Input.GetKey("F")) {     
            

            //Interacteble interacteble = hit.collider
        
        }*/

    }

    void FixedUpdate() {
        Run();
    }

    void Run() {

        if (dialogueBoxGUI.gameObject.active == false){

            if (Mathf.Abs(forwardInput) > inputDelay){
                //move
                rbody.velocity = transform.forward * forwardInput * forwardVel;
            }
            else{
                rbody.velocity = Vector3.zero;
            }
        }

    }

    void Turn()
    {

            //if (dialogueBoxGUI.gameObject.active == false)
            {
                targetRotation *= Quaternion.AngleAxis(rotateVel * turnInput * Time.deltaTime, Vector3.up);
                transform.rotation = targetRotation;
            }
    }



}
