﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorFader : MonoBehaviour
{
	// ENUMS
	public enum triggerType {OnAwake, OnEnter, OnExit};
	public enum fadeMode {FadeIn, FadeOut};

	// VARIABLES
	public GameObject fadingPanel;
	public Color color;
	public fadeMode mode = fadeMode.FadeOut;
	public triggerType condition = triggerType.OnEnter;
	public float duration;

	private Image image;
	private Color curColor;
	private float stepSize;
	private bool isTriggered = false;

	// METHODS
	void Start ()
	{
		initializeVariables ();
	}

	void Awake ()
	{
		initializeVariables ();

		if (condition == triggerType.OnAwake && !isTriggered)
		{
			isTriggered = true;

			if (mode == fadeMode.FadeIn)
				FadeIn ();
			else
				FadeOut ();
		}
	}

	void OnDisable ()
	{
		if (mode == fadeMode.FadeIn)
			curColor.a = 0.0f;
		else
			curColor.a = 1.0f;

		image.color = curColor;
	}

	void OnTriggerEnter (Collider col)
	{
		if (condition == triggerType.OnEnter && !isTriggered)
		{
			isTriggered = true;

			if (mode == fadeMode.FadeIn)
				FadeIn ();
			else
				FadeOut ();
		}
	}

	void OnTriggerExit (Collider col)
	{
		if (condition == triggerType.OnExit && !isTriggered)
		{
			isTriggered = true;

			if (mode == fadeMode.FadeIn)
				FadeIn ();
			else
				FadeOut ();
		}
	}

	private void initializeVariables ()
	{
		image = fadingPanel.GetComponent<Image>();
		image.color = color;
		curColor = color;

		stepSize = 1.0f / (duration * 60.0f);
	}
	
	public void FadeIn()
	{
		curColor.a = 1.0f;
		StartCoroutine (FadeInRoutine ());
	}

	public void FadeOut()
	{
		curColor.a = 0.0f;
		StartCoroutine (FadeOutRoutine ());
	}

	IEnumerator FadeInRoutine()
	{
		while (curColor.a > 0.0f)
		{
			curColor.a -= stepSize;
			image.color = curColor;

			yield return null;
		}
	}

	IEnumerator FadeOutRoutine()
	{
		while (image.color.a < 1.0f)
		{
			curColor.a += stepSize;
			image.color = curColor;

			yield return null;
		}
	}
}
