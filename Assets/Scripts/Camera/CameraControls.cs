﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraControls : MonoBehaviour
{
    // VARIABLES
	public PostProcessingProfile drunkProfileLow;
	public PostProcessingProfile drunkProfileMedium;
	public PostProcessingProfile drunkProfileHigh;
    
	public float mouseSensitivity = 5.0f;

	public bool enableWalking = false;
    public float walkingSpeed = 0.05f;

    private Vector3 newDirection;
    private Vector3 localRot;

	private PostProcessingBehaviour script_postPro;
	private InitDrunk script_initDrunk;

	// METHODS
	void Start ()
    {
        // initialize stuff
        localRot = new Vector3();

		script_postPro = gameObject.GetComponent<PostProcessingBehaviour> ();
		script_initDrunk = gameObject.GetComponent<InitDrunk> ();
	}
	
	void LateUpdate ()
	{
		// rotational inputs
		if (Input.GetAxis ("Mouse X") != 0 || Input.GetAxis ("Mouse Y") != 0)
		{
			// get current rotation
			localRot = gameObject.transform.eulerAngles;
			//Debug.Log (localRot);

			// get distance to rotate according to mouse on screen
			localRot.y += Input.GetAxis ("Mouse X") * mouseSensitivity;
			localRot.x -= Input.GetAxis ("Mouse Y") * mouseSensitivity;

			// clamp values
			//localRot.x = Mathf.Clamp (localRot.x, 20f, 340f);
			//localRot.y = Mathf.Clamp (localRot.y, -110f, 110f);

			// set new rotation
			gameObject.transform.rotation = Quaternion.Euler (localRot);
		}

		// walking inputs
		if (enableWalking)
		{
			newDirection = Vector3.zero;

			if (Input.GetKey (KeyCode.W)) {
				newDirection += gameObject.transform.forward;            
			}
			if (Input.GetKey (KeyCode.S)) {
				newDirection -= gameObject.transform.forward;
			}
			if (Input.GetKey (KeyCode.A)) {
				newDirection += Vector3.Cross (gameObject.transform.forward, gameObject.transform.up);
			}
			if (Input.GetKey (KeyCode.D)) {
				newDirection -= Vector3.Cross (gameObject.transform.forward, gameObject.transform.up);
			}

			// prevent up-/downward movement
			newDirection.y = 0f;
	        
			// normalize direction vector
			newDirection = Vector3.Normalize (newDirection);

			// update position
			gameObject.transform.position += newDirection * walkingSpeed;
		}

		// control post processing
		if (Input.GetKeyUp (KeyCode.F1))
		{
			// turn script on or off
			script_postPro.enabled = !script_postPro.enabled;
		}

		if (script_postPro.enabled && Input.GetKeyUp (KeyCode.F2))
		{
			script_postPro.profile = drunkProfileLow;
		}
		else if (script_postPro.enabled && Input.GetKeyUp (KeyCode.F3))
		{
			script_postPro.profile = drunkProfileMedium;
		}
		else if (script_postPro.enabled && Input.GetKeyUp (KeyCode.F4))
		{
			script_postPro.profile = drunkProfileHigh;
		}

		// de-/activate drunk shader
		if (Input.GetKeyUp (KeyCode.F5))
		{
			script_initDrunk.enabled = !script_initDrunk.enabled;
		}
    }
}
