﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class CarControlSwitch : MonoBehaviour
{
	// VARIABLES
	private CarAIControl ai;
	private CarUserControl user;

	// ENUMS
	public enum Controller {AI, User}

	// METHODS
	void Start()
	{
		ai = GetComponent<CarAIControl> ();
		user = GetComponent<CarUserControl> ();
	}

	public void SwitchControlTo(Controller ctrl)
	{
		switch (ctrl)
		{
			case Controller.AI:
				user.enabled = false;
				ai.enabled = true;
				break;

			case Controller.User:
				ai.enabled = false;
				user.enabled = true;
				break;

			default:
				break;
		}
	}
}
