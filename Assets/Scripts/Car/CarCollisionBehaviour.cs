﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class CarCollisionBehaviour : MonoBehaviour
{
	// VARIABLES
	private CarControlSwitch ctrlSwitch;
	private CarController carCtrl;

	// METHODS
	void Start()
	{
		ctrlSwitch = GetComponent<CarControlSwitch> ();
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.layer == 8)
			ctrlSwitch.SwitchControlTo (CarControlSwitch.Controller.AI);
	}

	void OnTriggerExit (Collider col)
	{
		if (col.gameObject.layer == 8)
			ctrlSwitch.SwitchControlTo (CarControlSwitch.Controller.User);
	}

	void OnTriggerStay (Collider col)
	{
		if (carCtrl.CurrentSpeed < 3)
			ctrlSwitch.SwitchControlTo (CarControlSwitch.Controller.User);
	}
}
