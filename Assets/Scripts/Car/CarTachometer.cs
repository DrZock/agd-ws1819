﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;

public class CarTachometer : MonoBehaviour
{
	// VARIABLES
	public Text display_kmh;

	private CarController carCtrl;

	// METHODS
	void Start ()
	{
		carCtrl = GetComponent<CarController> ();
	}

	void LateUpdate ()
	{
		display_kmh.text = (int) carCtrl.CurrentSpeed + " km/h";
	}
}
