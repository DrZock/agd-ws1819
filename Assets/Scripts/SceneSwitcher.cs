﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
	// ENUMS
	public enum triggerType {OnEnter, OnExit};

	// VARIABLES
	public string sceneName;
	public triggerType condition = triggerType.OnEnter;

	private bool isTriggered = false;

	// METHODS
	void OnTriggerEnter (Collider col)
	{
		if (condition == triggerType.OnEnter && !isTriggered)
		{
			isTriggered = true;
			SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
		}
	}

	void OnTriggerExit (Collider col)
	{
		if (condition == triggerType.OnExit && !isTriggered)
		{
			isTriggered = true;
			SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
		}
	}
}
