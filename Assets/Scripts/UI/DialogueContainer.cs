﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueContainer : MonoBehaviour
{
	// VARIABLES
	public GameObject targetPanel;
	public GameObject[] textObjs;
	public float delay = 0.01f;
	public float slowMotionFactor = 0.001f;
	public bool deactivateAfterTrigger = true;

	private Text curTextObj;
	private bool isTriggered = false;
	private int index = 0;

	// METHODS
	void LateUpdate ()
	{
		if (isTriggered)
		{
			HandleInput ();
		}
	}

	void OnTriggerEnter (Collider col)
	{
		if (!isTriggered)
		{
			// prevent multiple triggers
			isTriggered = true;

			// pause game
			Time.timeScale = slowMotionFactor;

			// activate first text object
			targetPanel.SetActive(true);
			textObjs [index].SetActive (true);
		}
	}

	void OnTriggerExit (Collider col)
	{
		// deactivate trigger object once it's used
		//gameObject.SetActive (false);
	}

	private void HandleInput()
	{
		if (Input.GetKeyDown(KeyCode.F))
		{
			// deactivate current text object
			textObjs [index].SetActive (false);

			// go to next entry
			index++;

			if (index < textObjs.Length)
			{
				// activate next text object
				textObjs [index].SetActive (true);
			}
			else
			{
				// reset
				index = 0;
				isTriggered = false;

				// deactivate screen space panel
				targetPanel.SetActive(false);

				// deactivate trigger object
				if (deactivateAfterTrigger)
				{					
					gameObject.SetActive (false);
				}

				// resume game
				Time.timeScale = 1.0f;
			}
		}
	}
}
