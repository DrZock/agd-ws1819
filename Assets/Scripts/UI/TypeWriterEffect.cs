﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TypeWriterEffect : MonoBehaviour
{
	// VARIABLES
	public float delay = 0.01f;

	private string currentString = "";
	private string fullString;
	private bool stringComplete = false;
	private Text textObj;

	// METHODS
	void Awake ()
	{
		textObj = GetComponent<Text> ();
		fullString = textObj.text;
		StartCoroutine (ShowText(textObj));
	}

	void Update ()
	{
		// handle player input
		if (Input.GetKeyDown (KeyCode.F))
		{
			// handle completed string
			if (stringComplete)
			{
				// revert bool
				stringComplete = false;

				// remove finished text
				gameObject.SetActive (false);

				// resume game
				Time.timeScale = 1.0f;
			}

			// skip type writer effect and display whole string
			textObj.text = fullString;
			stringComplete = true;
		}
	}

	IEnumerator ShowText(Text textObj)
	{
		// pause game
		Time.timeScale = 0.001f;

		for (int i = 0; i < fullString.Length; i++)
		{
			// write next letter of full string
			if (!stringComplete)
			{
				currentString = fullString.Substring (0, i);
				textObj.text = currentString;

				// check for completed string
				if (i + 1 == fullString.Length)
				{
					textObj.text = fullString;
					stringComplete = true;
				}

				yield return new WaitForSeconds (delay/1000);
			}
		}
	}
}
