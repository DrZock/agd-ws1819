﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSwitcher : MonoBehaviour
{
	// ENUMS
	public enum triggerType {OnAwake, OnDisable, OnEnter, OnExit};

	// VARIABLES
	public GameObject[] objsToActivate;
	public GameObject[] objsToDeactivate;
	public triggerType condition = triggerType.OnEnter;

	private bool isTriggered = false;

	// METHODS
	void Awake ()
	{
		if (condition == triggerType.OnAwake && !isTriggered)
		{
			isTriggered = true;
			switchObjects ();
		}
	}

	void OnDisable ()
	{
		if (condition == triggerType.OnDisable && !isTriggered)
		{
			isTriggered = true;
			switchObjects ();
		}
	}

	void OnTriggerEnter (Collider col)
	{
		if (condition == triggerType.OnEnter && !isTriggered)
		{
			isTriggered = true;
			switchObjects ();
		}
	}

	void OnTriggerExit (Collider col)
	{
		if (condition == triggerType.OnExit && !isTriggered)
		{
			isTriggered = true;
			switchObjects ();
		}
	}

	private void switchObjects ()
	{
		// activate objects
		for (int i = 0; i < objsToActivate.Length; i++)
		{
			if (!objsToActivate[i].Equals(null))
				objsToActivate [i].SetActive (true);
		}

		// deactivate objects
		for (int i = 0; i < objsToDeactivate.Length; i++)
		{
			if (!objsToDeactivate[i].Equals(null))
				objsToDeactivate [i].SetActive (false);
		}
	}
}
