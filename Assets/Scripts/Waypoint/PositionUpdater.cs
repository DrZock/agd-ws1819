﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionUpdater : MonoBehaviour
{
	// VARIABLES
	public Transform targetTransform;

	// METHODS
	void LateUpdate ()
	{
		this.transform.position = targetTransform.position;
	}
}
